#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#import "md5.h"

int main(int argc, char *argv[])
{
    if(argc < 3)
    {
        printf("You must enter two filenames\n");
        return 1;
    }
    else
    {
        FILE *f = fopen( argv[1], "r");
        FILE *s = fopen( argv[2], "w");
        if(!f)
        {
            printf("Couldn't open file\n");
            exit(1);
        }
        char password[15];
        while( fscanf(f,"%s", password) != EOF )
        {
            int pw_length = strlen(password);
            char *p = md5(password, pw_length);
            fprintf(s, "%s\n", p);
        } 
        fclose(s);  
        fclose(f);
    }
}    